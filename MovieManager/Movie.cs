﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieManager
{
	class Movie : BaseEntity
	{
		public string Name { get; set; }

		public DateTime DateRelease { get; set; }

		public string Author { get; set; }
	}
}
