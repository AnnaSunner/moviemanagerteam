﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieManager
{
	interface IRepository
	{
		void Add<TEntity>(TEntity entity) where TEntity : BaseEntity;

		List<TEntity> GetList<TEntity>(TEntity entity) where TEntity : BaseEntity;		
	}
}
